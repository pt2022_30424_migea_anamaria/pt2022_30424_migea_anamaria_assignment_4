package presentation;

import start.Start;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {
    private View view;

    public Controller(View v){

        this.view = v;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if(command.equals("CLIENT_OP") ) {
            view.closeMainFrame();

            ViewClient viewClient = new ViewClient("Food delivery management system", view);
            viewClient.setVisible(true);

        }else if(command.equals("ADMINISTRATOR_OP")){
            view.closeMainFrame();

            ViewAdministrator viewAdministrator = new ViewAdministrator("Food delivery management system",view);
            viewAdministrator.setVisible(true);

        }else if(command.equals("EMPLOYEE_OP")){
            //view.closeMainFrame();

            ViewEmployee viewEmployee = new ViewEmployee("Employees' frame");
            viewEmployee.setVisible(true);

            Start.getDeliveryService().addObserver(viewEmployee);

        }
    }
}
