package presentation;

import business.Client;
import business.MenuItem;
import start.Start;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ControllerClient implements ActionListener {


    private ViewClient view;

    private Client client;

    public ControllerClient(ViewClient v){
        this.view = v;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "LOG_IN_CLIENT" -> {
                String usernameString = String.valueOf(view.getUsernameTextField().getText());
                String passwordString = String.valueOf(view.getPasswordTextField().getText());

                client = Start.getDeliveryService().userExists(usernameString);
                if (client == null) {
                    JOptionPane.showMessageDialog(this.view, "You have not an account yet\nPlease register first");
                    view.getRegisterButton().setEnabled(true);
                    view.getLoginButton().setEnabled(false);
                } else {
                    if (client.getPassword().equals(passwordString)) {
                        JOptionPane.showMessageDialog(this.view, "Access granted! Welcome!");
                        view.removeFromPane();
                        view.initializeTable(Start.getDeliveryService().getMenuItems());
                        view.initializeButtonsAndLabels();
                    } else {
                        JOptionPane.showMessageDialog(null, "Invalid password!", "ERROR Authentication", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            }
            case "REGISTER_CLIENT" -> {
                String usernameString = String.valueOf(view.getUsernameTextField().getText());
                String passwordString = String.valueOf(view.getPasswordTextField().getText());
                Client newClient = new Client(usernameString, passwordString);

                Client client = Start.getDeliveryService().userExists(usernameString);
                if (client == null) {
                    Start.getDeliveryService().addClient(newClient);
                    JOptionPane.showMessageDialog(this.view, "You have successfully created an account\nNow login");
                    view.getRegisterButton().setEnabled(false);
                    view.getLoginButton().setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(this.view, "This username already exists\nPlease choose another");
                }

                break;
            }
            case "VIEW_MENU" -> view.initializeTable(Start.getDeliveryService().getMenuItems());
            case "SEARCH_BY_CRITERIA" -> {
                String title = String.valueOf(view.getTitleTextField().getText());
                String ratingString = String.valueOf(view.getRatingTextField().getText());
                String caloriesString = String.valueOf(view.getCaloriesTextField().getText());
                String proteinString = String.valueOf(view.getProteinTextField().getText());
                String fatString = String.valueOf(view.getFatTextField().getText());
                String sodiumString = String.valueOf(view.getSodiumTextField().getText());
                String priceString = String.valueOf(view.getPriceTextField().getText());
                double rating = 0.0, price = 0.0;
                int calories = 0, protein = 0, fat = 0, sodium = 0;
                try {
                    rating = Double.parseDouble(ratingString);
                } catch (NumberFormatException numberFormatException) {
                    rating = -1;
                }
                try {
                    calories = Integer.parseInt(caloriesString);
                } catch (NumberFormatException numberFormatException) {
                    calories = -1;
                }
                try {
                    protein = Integer.parseInt(proteinString);
                } catch (NumberFormatException numberFormatException) {
                    protein = -1;
                }
                try {
                    fat = Integer.parseInt(fatString);
                } catch (NumberFormatException numberFormatException) {
                    fat = -1;
                }
                try {
                    sodium = Integer.parseInt(sodiumString);
                } catch (NumberFormatException numberFormatException) {
                    sodium = -1;
                }
                try {
                    price = Double.parseDouble(priceString);
                } catch (NumberFormatException numberFormatException) {
                    price = -1;
                }
                List<MenuItem> foundMenuItems = Start.getDeliveryService().searchByCriteria(title, rating, calories, protein, fat, sodium, price);
                view.initializeTable(foundMenuItems);
            }
            case "MAKE_ORDER" -> {
                List<String> composedOf = new ArrayList<>();
                if (view.getTable().getSelectedRowCount() > 0) {
                    int[] selectedRow = view.getTable().getSelectedRows();
                    for (int i : selectedRow) {
                        composedOf.add(view.getTable().getValueAt(i, 0).toString());
                    }
                    Start.getDeliveryService().addOrder(getClient(), composedOf);
                    JOptionPane.showMessageDialog(this.view, "Order successfully placed");
                } else {
                    JOptionPane.showMessageDialog(this.view, "Select alt least one row!", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    public Client getClient() {
        return client;
    }
}
