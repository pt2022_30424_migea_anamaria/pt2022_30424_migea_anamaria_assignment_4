package presentation;

import business.Order;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class ViewEmployee extends JFrame implements Observer {

    private JPanel contentPane;
    private JPanel resultPanel;

    private JTextArea textArea;

    public ViewEmployee(String name) {
        super(name);
        this.prepareGui();
        this.setResizable(false);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Order){
            textArea.append(((Order)arg).toString());
        }
    }

    private void prepareGui(){
        this.contentPane = new JPanel();
        this.resultPanel = new JPanel();

        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.contentPane.setLayout(null);

        this.resultPanel.setLayout(null);
        this.resultPanel.setBounds(20,10,450,450);

        this.textArea = new JTextArea();
        this.textArea.setEditable(false);
        Font font = new Font("Times New Roman", Font.BOLD, 15);
        this.textArea.setFont(font);
        JScrollPane scroll = new JScrollPane(this.textArea);

        this.resultPanel.add(scroll);
        this.resultPanel.setLayout(new GridLayout(1,1));

        this.contentPane.add(this.resultPanel);

        this.setContentPane(this.contentPane);
    }
}
