package presentation;

import business.MenuItem;
import data.ReflectionJTable;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class ViewClient extends JFrame {

    private JPanel contentPane;
    private JPanel tablePane;

    private JTable table;

    private JLabel usernameLabel;
    private JTextField usernameTextField;
    private JLabel passwordLabel;
    private JPasswordField passwordTextField;
    private JButton loginButton;
    private JButton registerButton;

    private JLabel titleLabel;
    private JTextField titleTextField;
    private JLabel ratingLabel;
    private JTextField ratingTextField;
    private JLabel caloriesLabel;
    private JTextField caloriesTextField;
    private JLabel proteinLabel;
    private JTextField proteinTextField;
    private JLabel fatLabel;
    private JTextField fatTextField;
    private JLabel sodiumLabel;
    private JTextField sodiumTextField;
    private JLabel priceLabel;
    private JTextField priceTextField;

    private JButton searchByCriteriaButton;
    private JButton viewMenuButton;
    private JButton makeOrderButton;

    private View previousView;
    protected ControllerClient controllerClient = new ControllerClient(this);
    public ViewClient(String name, View previousView) {
        super(name);
        this.previousView = previousView;
        this.prepareGui();
        this.setResizable(false);

        this.addWindowListener(new WindowAdapter() {
            //for closing
            @Override
            public void windowClosing(WindowEvent e) {
                previousView.revealMainFrame();
            }
        });

    }

    private void prepareGui(){
        this.contentPane = new JPanel();
        this.tablePane = new JPanel();

        this.setSize(1500, 500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.contentPane.setLayout(null);

        this.tablePane.setLayout(null);
        this.tablePane.setBounds(700,10,780,400);

        this.usernameLabel = new JLabel("username =");
        this.usernameLabel.setBounds(100, 100, 100,30);
        this.contentPane.add(this.usernameLabel);
        this.usernameTextField = new JTextField();
        this.usernameTextField.setBounds(180, 100, 200,30);
        this.usernameTextField.setVisible(true);
        this.contentPane.add(this.usernameTextField);

        this.passwordLabel = new JLabel("password =");
        this.passwordLabel.setBounds(100, 200, 100,30);
        this.contentPane.add(this.passwordLabel);
        this.passwordTextField = new JPasswordField();
        this.passwordTextField.setBounds(180, 200, 200,30);
        this.passwordTextField.setVisible(true);
        this.contentPane.add(this.passwordTextField);

        this.loginButton = new JButton();
        this.loginButton.setText("LOG IN");
        this.loginButton.setBounds(350, 350, 100,30);
        this.loginButton.setActionCommand("LOG_IN_CLIENT");
        this.loginButton.addActionListener(this.controllerClient);
        this.contentPane.add(this.loginButton);

        this.registerButton = new JButton();
        this.registerButton.setText("REGISTER");
        this.registerButton.setBounds(150, 350, 100,30);
        this.registerButton.setActionCommand("REGISTER_CLIENT");
        this.registerButton.addActionListener(this.controllerClient);
        this.registerButton.setEnabled(false);
        this.contentPane.add(this.registerButton);

        this.titleLabel = new JLabel("title =");
        this.titleTextField = new JTextField();
        this.titleLabel.setBounds(250, 10, 50,30);
        this.titleTextField.setBounds(320, 10, 350,30);

        this.ratingLabel = new JLabel("rating =");
        this.ratingTextField = new JTextField();
        this.ratingLabel.setBounds(250, 50, 50,30);
        this.ratingTextField.setBounds(320, 50, 200,30);

        this.caloriesLabel = new JLabel("calories =");
        this.caloriesTextField = new JTextField();
        this.caloriesLabel.setBounds(250, 90, 60,30);
        this.caloriesTextField.setBounds(320, 90, 200,30);

        this.proteinLabel = new JLabel("protein =");
        this.proteinTextField = new JTextField();
        this.proteinLabel.setBounds(250, 130, 60,30);
        this.proteinTextField.setBounds(320, 130, 200,30);

        this.fatLabel = new JLabel("fat =");
        this.fatTextField = new JTextField();
        this.fatLabel.setBounds(250, 170, 50,30);
        this.fatTextField.setBounds(320, 170, 200,30);

        this.sodiumLabel = new JLabel("sodium =");
        this.sodiumTextField = new JTextField();
        this.sodiumLabel.setBounds(250, 210, 60,30);
        this.sodiumTextField.setBounds(320, 210, 200,30);

        this.priceLabel = new JLabel("price =");
        this.priceTextField = new JTextField();
        this.priceLabel.setBounds(250, 250, 50,30);
        this.priceTextField.setBounds(320, 250, 200,30);

        this.viewMenuButton = new JButton();
        this.viewMenuButton.setText("VIEW MENU");
        this.viewMenuButton.setBounds(10, 10, 200,30);
        this.viewMenuButton.setActionCommand("VIEW_MENU");
        this.viewMenuButton.addActionListener(this.controllerClient);

        this.searchByCriteriaButton = new JButton();
        this.searchByCriteriaButton.setText("SEARCH BY CRITERIA");
        this.searchByCriteriaButton.setBounds(10, 50, 200,30);
        this.searchByCriteriaButton.setActionCommand("SEARCH_BY_CRITERIA");
        this.searchByCriteriaButton.addActionListener(this.controllerClient);

        this.makeOrderButton = new JButton();
        this.makeOrderButton.setText("MAKE ORDER");
        this.makeOrderButton.setBounds(10, 90, 200,30);
        this.makeOrderButton.setActionCommand("MAKE_ORDER");
        this.makeOrderButton.addActionListener(this.controllerClient);

        this.setContentPane(this.contentPane);

    }

    public void initializeButtonsAndLabels(){

        this.contentPane.add(this.titleLabel);
        this.contentPane.add(this.titleTextField);
        this.contentPane.add(this.ratingLabel);
        this.contentPane.add(this.ratingTextField);
        this.contentPane.add(this.caloriesLabel);
        this.contentPane.add(this.caloriesTextField);
        this.contentPane.add(this.proteinLabel);
        this.contentPane.add(this.proteinTextField);
        this.contentPane.add(this.fatLabel);
        this.contentPane.add(this.fatTextField);
        this.contentPane.add(this.sodiumLabel);
        this.contentPane.add(this.sodiumTextField);
        this.contentPane.add(this.priceLabel);
        this.contentPane.add(this.priceTextField);
        this.contentPane.add(this.viewMenuButton);
        this.contentPane.add(this.searchByCriteriaButton);
        this.contentPane.add(this.makeOrderButton);

    }

    public void initializeTable(List<MenuItem> menuItems){

        ReflectionJTable reflexionJTable = new ReflectionJTable();
        table = new JTable();
        table = reflexionJTable.retrieveTable(menuItems);

        prepareTable(table);

    }

    private void prepareTable(JTable tableArg){
        this.tablePane.removeAll();

        JTableHeader header = tableArg.getTableHeader();
        header.setBackground(Color.yellow);
        JScrollPane pane = new JScrollPane(tableArg);
        tableArg.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableArg.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableArg.getColumnModel().getColumn(1).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(2).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(3).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(5).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(6).setPreferredWidth(60);

        this.tablePane.add(pane);
        this.table =tableArg;

        this.tablePane.setLayout(new GridLayout(1,1));
        this.contentPane.add(this.tablePane);
        this.setContentPane(this.contentPane);
    }

    public JTextField getUsernameTextField() {
        return usernameTextField;
    }

    public JPasswordField getPasswordTextField() {
        return passwordTextField;
    }

    public JButton getRegisterButton() {
        return registerButton;
    }

    public JButton getLoginButton() {
        return loginButton;
    }

    public void removeFromPane(){
        this.contentPane.removeAll();
        this.setContentPane(this.contentPane);
    }

    public JTable getTable() {
        return table;
    }

    public JTextField getTitleTextField() {
        return titleTextField;
    }

    public JTextField getRatingTextField() {
        return ratingTextField;
    }

    public JTextField getCaloriesTextField() {
        return caloriesTextField;
    }

    public JTextField getProteinTextField() {
        return proteinTextField;
    }

    public JTextField getFatTextField() {
        return fatTextField;
    }

    public JTextField getSodiumTextField() {
        return sodiumTextField;
    }

    public JTextField getPriceTextField() {
        return priceTextField;
    }
}
