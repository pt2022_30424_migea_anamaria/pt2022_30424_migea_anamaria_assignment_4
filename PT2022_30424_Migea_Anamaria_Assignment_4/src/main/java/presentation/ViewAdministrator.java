package presentation;

import business.MenuItem;
import data.ReflectionJTable;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class ViewAdministrator extends JFrame {

    private JPanel contentPane;
    private JPanel tablePane;

    private JTable table;

    private JLabel usernameLabel;
    private JTextField usernameTextField;
    private JLabel passwordLabel;
    private JPasswordField passwordTextField;
    private JButton loginButton;

    private JLabel titleLabel;
    private JTextField titleTextField;
    private JLabel ratingLabel;
    private JTextField ratingTextField;
    private JLabel caloriesLabel;
    private JTextField caloriesTextField;
    private JLabel proteinLabel;
    private JTextField proteinTextField;
    private JLabel fatLabel;
    private JTextField fatTextField;
    private JLabel sodiumLabel;
    private JTextField sodiumTextField;
    private JLabel priceLabel;
    private JTextField priceTextField;

    private JButton importProductsButton;
    private JButton deleteRowButton;
    private JButton modifyProductButton;
    private JButton addBaseProductButton;
    private JButton addCompositeProductButton;

    private JButton firstReportButton;
    private JButton secondReportButton;
    private JButton thirdReportButton;
    private JButton fourthReportButton;
    private JLabel firstReportLabel;
    private JLabel secondReportLabel;
    private JLabel thirdReportLabel;
    private JLabel fourthReportLabel;

    private JTextField reportFirstTextField;
    private JTextField reportSecondTextField;


    private View previousView;
    protected ControllerAdministrator controllerAdministrator = new ControllerAdministrator(this);

    public ViewAdministrator(String name, View previousView) {
        super(name);
        this.previousView = previousView;
        this.prepareGui();
        this.setResizable(false);

        this.addWindowListener(new WindowAdapter() {
            //for closing
            @Override
            public void windowClosing(WindowEvent e) {
                previousView.revealMainFrame();
            }
        });

    }

    private void prepareGui(){
        this.contentPane = new JPanel();
        this.tablePane = new JPanel();

        this.setSize(1500, 600);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.contentPane.setLayout(null);

        this.tablePane.setLayout(null);
        this.tablePane.setBounds(700,10,780,400);

        this.usernameLabel = new JLabel("username =");
        this.usernameLabel.setBounds(100, 100, 100,30);
        this.contentPane.add(this.usernameLabel);
        this.usernameTextField = new JTextField();
        this.usernameTextField.setBounds(180, 100, 200,30);
        this.usernameTextField.setVisible(true);
        this.contentPane.add(this.usernameTextField);

        this.passwordLabel = new JLabel("password =");
        this.passwordLabel.setBounds(100, 200, 100,30);
        this.contentPane.add(this.passwordLabel);
        this.passwordTextField = new JPasswordField();
        this.passwordTextField.setBounds(180, 200, 200,30);
        this.passwordTextField.setVisible(true);
        this.contentPane.add(this.passwordTextField);

        this.loginButton = new JButton();
        this.loginButton.setText("LOG IN");
        this.loginButton.setBounds(350, 350, 100,30);
        this.loginButton.setActionCommand("LOG_IN_ADMINISTRATOR");
        this.loginButton.addActionListener(this.controllerAdministrator);
        this.contentPane.add(this.loginButton);

        this.importProductsButton = new JButton();
        this.importProductsButton.setText("IMPORT PRODUCTS");
        this.importProductsButton.setBounds(10, 10, 200,30);
        this.importProductsButton.setActionCommand("IMPORT_PRODUCTS");
        this.importProductsButton.addActionListener(this.controllerAdministrator);

        this.deleteRowButton = new JButton();
        this.deleteRowButton.setText("DELETE PRODUCT");
        this.deleteRowButton.setBounds(10, 50, 200,30);
        this.deleteRowButton.setActionCommand("DELETE_ROW");
        this.deleteRowButton.addActionListener(this.controllerAdministrator);

        this.modifyProductButton = new JButton();
        this.modifyProductButton.setText("MODIFY PRODUCT");
        this.modifyProductButton.setBounds(10, 90, 200,30);
        this.modifyProductButton.setActionCommand("MODIFY_PRODUCT");
        this.modifyProductButton.addActionListener(this.controllerAdministrator);

        this.addBaseProductButton = new JButton();
        this.addBaseProductButton.setText("ADD NEW BASE PRODUCT");
        this.addBaseProductButton.setBounds(10, 130, 200,30);
        this.addBaseProductButton.setActionCommand("ADD_BASE_PRODUCT");
        this.addBaseProductButton.addActionListener(this.controllerAdministrator);

        this.addCompositeProductButton = new JButton();
        this.addCompositeProductButton.setText("ADD NEW COMPOSITE PRODUCT");
        this.addCompositeProductButton.setBounds(10, 170, 230,30);
        this.addCompositeProductButton.setActionCommand("ADD_COMPOSITE_PRODUCT");
        this.addCompositeProductButton.addActionListener(this.controllerAdministrator);

        this.firstReportButton = new JButton();
        this.firstReportButton.setText("GENERATE FIRST REPORT");
        this.firstReportButton.setBounds(10, 400, 230,30);
        this.firstReportButton.setActionCommand("GENERATE_FIRST_REPORT");
        this.firstReportButton.addActionListener(this.controllerAdministrator);

        this.secondReportButton = new JButton();
        this.secondReportButton.setText("GENERATE SECOND REPORT");
        this.secondReportButton.setBounds(10, 440, 230,30);
        this.secondReportButton.setActionCommand("GENERATE_SECOND_REPORT");
        this.secondReportButton.addActionListener(this.controllerAdministrator);

        this.thirdReportButton = new JButton();
        this.thirdReportButton.setText("GENERATE THIRD REPORT");
        this.thirdReportButton.setBounds(10, 480, 230,30);
        this.thirdReportButton.setActionCommand("GENERATE_THIRD_REPORT");
        this.thirdReportButton.addActionListener(this.controllerAdministrator);

        this.fourthReportButton= new JButton();
        this.fourthReportButton.setText("GENERATE FOURTH REPORT");
        this.fourthReportButton.setBounds(10, 520, 230,30);
        this.fourthReportButton.setActionCommand("GENERATE_FOURTH_REPORT");
        this.fourthReportButton.addActionListener(this.controllerAdministrator);

        this.reportFirstTextField = new JTextField();
        this.reportFirstTextField.setBounds(350, 430, 200,30);
        this.reportSecondTextField = new JTextField();
        this.reportSecondTextField.setBounds(350, 470, 200,30);

        this.firstReportLabel = new JLabel("First report: generates the orders performed between a given start hour and a given end hour regardless the date.");
        this.firstReportLabel.setBounds(570, 410, 700,20);
        this.secondReportLabel = new JLabel("Second report: generates the products ordered more than a specified number of times so far.");
        this.secondReportLabel.setBounds(570,430,700,20);
        this.thirdReportLabel = new JLabel("Third report: generates the clients that have ordered more than a specified number of times so far and the " +
                "value of the order was higher than a specified amount.");
        this.thirdReportLabel.setBounds(570,450,910,20);
        this.fourthReportLabel = new JLabel("Fourth report: generates the products ordered within a specified day with the number of times they have been ordered.");
        this.fourthReportLabel.setBounds(570,470,700,20);

        this.titleLabel = new JLabel("title =");
        this.titleTextField = new JTextField();
        this.titleLabel.setBounds(250, 10, 50,30);
        this.titleTextField.setBounds(320, 10, 350,30);

        this.ratingLabel = new JLabel("rating =");
        this.ratingTextField = new JTextField();
        this.ratingLabel.setBounds(250, 50, 50,30);
        this.ratingTextField.setBounds(320, 50, 200,30);

        this.caloriesLabel = new JLabel("calories =");
        this.caloriesTextField = new JTextField();
        this.caloriesLabel.setBounds(250, 90, 60,30);
        this.caloriesTextField.setBounds(320, 90, 200,30);

        this.proteinLabel = new JLabel("protein =");
        this.proteinTextField = new JTextField();
        this.proteinLabel.setBounds(250, 130, 60,30);
        this.proteinTextField.setBounds(320, 130, 200,30);

        this.fatLabel = new JLabel("fat =");
        this.fatTextField = new JTextField();
        this.fatLabel.setBounds(250, 170, 50,30);
        this.fatTextField.setBounds(320, 170, 200,30);

        this.sodiumLabel = new JLabel("sodium =");
        this.sodiumTextField = new JTextField();
        this.sodiumLabel.setBounds(250, 210, 60,30);
        this.sodiumTextField.setBounds(320, 210, 200,30);

        this.priceLabel = new JLabel("price =");
        this.priceTextField = new JTextField();
        this.priceLabel.setBounds(250, 250, 50,30);
        this.priceTextField.setBounds(320, 250, 200,30);

        this.importProductsButton = new JButton();
        this.importProductsButton.setText("IMPORT PRODUCTS");
        this.importProductsButton.setBounds(10, 10, 200,30);
        this.importProductsButton.setActionCommand("IMPORT_PRODUCTS");
        this.importProductsButton.addActionListener(this.controllerAdministrator);


        this.setContentPane(this.contentPane);
    }

    public void initializeButtonsAndLabels(){
        this.contentPane.add(this.importProductsButton);
        this.contentPane.add(this.deleteRowButton);
        this.contentPane.add(this.modifyProductButton);
        this.contentPane.add(this.titleLabel);
        this.contentPane.add(this.titleTextField);
        this.contentPane.add(this.ratingLabel);
        this.contentPane.add(this.ratingTextField);
        this.contentPane.add(this.caloriesLabel);
        this.contentPane.add(this.caloriesTextField);
        this.contentPane.add(this.proteinLabel);
        this.contentPane.add(this.proteinTextField);
        this.contentPane.add(this.fatLabel);
        this.contentPane.add(this.fatTextField);
        this.contentPane.add(this.sodiumLabel);
        this.contentPane.add(this.sodiumTextField);
        this.contentPane.add(this.priceLabel);
        this.contentPane.add(this.priceTextField);
        this.contentPane.add(this.addBaseProductButton);
        this.contentPane.add(this.addCompositeProductButton);
        this.contentPane.add(this.firstReportButton);
        this.contentPane.add(this.secondReportButton);
        this.contentPane.add(this.thirdReportButton);
        this.contentPane.add(this.fourthReportButton);
        this.contentPane.add(this.reportFirstTextField);
        this.contentPane.add(this.reportSecondTextField);
        this.contentPane.add(this.firstReportLabel);
        this.contentPane.add(this.secondReportLabel);
        this.contentPane.add(this.thirdReportLabel);
        this.contentPane.add(this.fourthReportLabel);
    }

    public void initializeTable(List<MenuItem> menuItems){

        ReflectionJTable reflexionJTable = new ReflectionJTable();
        table = new JTable();
        table = reflexionJTable.retrieveTable(menuItems);

        prepareTable(table);
        this.getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                String titleString = (String) getTable().getValueAt(getTable().getSelectedRow(), 0);
                double rating = (double) getTable().getValueAt(getTable().getSelectedRow(), 1);
                int calories = (int) getTable().getValueAt(getTable().getSelectedRow(), 2);
                int protein = (int) getTable().getValueAt(getTable().getSelectedRow(), 3);
                int fat = (int) getTable().getValueAt(getTable().getSelectedRow(), 4);
                int sodium = (int) getTable().getValueAt(getTable().getSelectedRow(), 5);
                double price = (double) getTable().getValueAt(getTable().getSelectedRow(), 6);
                getTitleTextField().setText(titleString);
                getRatingTextField().setText(String.valueOf(rating));
                getCaloriesTextField().setText(String.valueOf(calories));
                getProteinTextField().setText(String.valueOf(protein));
                getFatTextField().setText(String.valueOf(fat));
                getSodiumTextField().setText(String.valueOf(sodium));
                getPriceTextField().setText(String.valueOf(price));
            }
        });
    }

    private void prepareTable(JTable tableArg){
        this.tablePane.removeAll();

        JTableHeader header = tableArg.getTableHeader();
        header.setBackground(Color.yellow);
        JScrollPane pane = new JScrollPane(tableArg);
        tableArg.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableArg.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableArg.getColumnModel().getColumn(1).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(2).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(3).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(5).setPreferredWidth(60);
        tableArg.getColumnModel().getColumn(6).setPreferredWidth(60);

        this.tablePane.add(pane);
        this.table =tableArg;

        this.tablePane.setLayout(new GridLayout(1,1));
        this.contentPane.add(this.tablePane);
        this.setContentPane(this.contentPane);
    }

    public JTextField getUsernameTextField() {
        return usernameTextField;
    }

    public JPasswordField getPasswordTextField() {
        return passwordTextField;
    }

    public void removeFromPane(){
        this.contentPane.removeAll();
        this.setContentPane(this.contentPane);
    }

    public JTable getTable() {
        return table;
    }

    public JTextField getTitleTextField() {
        return titleTextField;
    }

    public JTextField getRatingTextField() {
        return ratingTextField;
    }

    public JTextField getCaloriesTextField() {
        return caloriesTextField;
    }

    public JTextField getProteinTextField() {
        return proteinTextField;
    }

    public JTextField getFatTextField() {
        return fatTextField;
    }

    public JTextField getSodiumTextField() {
        return sodiumTextField;
    }

    public JTextField getPriceTextField() {
        return priceTextField;
    }

    public JTextField getReportFirstTextField() {
        return reportFirstTextField;
    }

    public JTextField getReportSecondTextField() {
        return reportSecondTextField;
    }
}
