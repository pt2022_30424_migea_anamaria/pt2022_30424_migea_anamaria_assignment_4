package presentation;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame {

    private JPanel contentPane;

    private JButton clientButton;
    private JButton administratorButton;
    private JButton employeeButton;

    private ImageIcon ordersIcon;
    private JLabel myImageLabel;

    protected Controller controller = new Controller(this);

    public View(String name) {
        super(name);
        this.prepareGui();
        this.setResizable(false);
    }

    private void prepareGui(){
        myImageLabel = new JLabel();
        myImageLabel.setIcon(ordersIcon);
        myImageLabel.setSize(500,500);

        this.contentPane = new JPanel();
        ordersIcon = new ImageIcon(this.getClass().getResource("/orders.jpg"));
        Image img = ordersIcon.getImage();
        Image imgScale = img.getScaledInstance(myImageLabel.getWidth(),myImageLabel.getHeight(),Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(imgScale);
        myImageLabel.setIcon(scaledIcon);

        this.setSize(500, 500);
        // here's the part where I center the frame on screen
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.contentPane.setLayout(null);


        Font fontButtons = new Font("Times New Roman", Font.BOLD, 15);

        this.clientButton = new JButton("Client");
        this.clientButton.setBounds(200, 80, 100,30);
        this.clientButton.setFont(fontButtons);
        this.clientButton.setActionCommand("CLIENT_OP");
        this.clientButton.addActionListener(this.controller);
        this.contentPane.add(this.clientButton);

        this.administratorButton = new JButton("Administrator");
        this.administratorButton.setBounds(180, 140, 140,30);
        this.administratorButton.setFont(fontButtons);
        this.administratorButton.setActionCommand("ADMINISTRATOR_OP");
        this.administratorButton.addActionListener(this.controller);
        this.contentPane.add(this.administratorButton);

        this.employeeButton = new JButton("Employee");
        this.employeeButton.setBounds(200, 200, 100,30);
        this.employeeButton.setFont(fontButtons);
        this.employeeButton.setActionCommand("EMPLOYEE_OP");
        this.employeeButton.addActionListener(this.controller);
        this.contentPane.add(this.employeeButton);

        this.contentPane.add(myImageLabel);
        this.setContentPane(this.contentPane);
    }

    public void closeMainFrame(){
        setVisible(false);
    }

    public void revealMainFrame(){
        setVisible(true);
    }

}
