package presentation;

import business.MenuItem;
import business.Order;
import start.Start;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ControllerAdministrator implements ActionListener {
    private final String ADMINISTRATOR_USERNAME = "admin";
    private final String ADMINISTRATOR_PASSWORD = "admin";

    private ViewAdministrator view;

    public ControllerAdministrator(ViewAdministrator v){

        this.view = v;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        List<MenuItem> menuItems;
        switch (command) {
            case "LOG_IN_ADMINISTRATOR":
                String usernameString = String.valueOf(view.getUsernameTextField().getText());
                String passwordString = String.valueOf(view.getPasswordTextField().getText());

                if (usernameString.equals(ADMINISTRATOR_USERNAME) && passwordString.equals(ADMINISTRATOR_PASSWORD)) {
                    JOptionPane.showMessageDialog(this.view, "Access granted! Welcome!");
                    view.removeFromPane();
                    view.initializeTable(Start.getDeliveryService().getMenuItems());
                    view.initializeButtonsAndLabels();
                } else if (usernameString.equals(ADMINISTRATOR_USERNAME)) {
                    JOptionPane.showMessageDialog(this.view, "Invalid password!", "ERROR Authentication", JOptionPane.ERROR_MESSAGE);
                } else if (passwordString.equals(ADMINISTRATOR_PASSWORD)) {
                    JOptionPane.showMessageDialog(this.view, "Invalid username!", "ERROR Authentication", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this.view, "Invalid username and password!", "ERROR Authentication", JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "IMPORT_PRODUCTS":
                menuItems = Start.getDeliveryService().importInitialProducts();
                view.initializeTable(Start.getDeliveryService().getMenuItems());
                break;
            case "DELETE_ROW":
                DefaultTableModel defaultTableModel = (DefaultTableModel) view.getTable().getModel();
                if (view.getTable().getSelectedRowCount() == 1) {
                    String title = (String) view.getTable().getValueAt(view.getTable().getSelectedRow(), 0);
                    defaultTableModel.removeRow(view.getTable().getSelectedRow());
                    Start.getDeliveryService().removeMenuItem(title);
                } else if (defaultTableModel.getRowCount() == 0) {
                    JOptionPane.showMessageDialog(this.view, "Table is empty!", "ERROR Delete from table", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this.view, "Please select a single row to delete!", "ERROR Delete from table", JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "MODIFY_PRODUCT":
                if (view.getTable().getSelectedRowCount() == 1) {
                    String initialTitle = (String) view.getTable().getValueAt(view.getTable().getSelectedRow(), 0);
                    String title = String.valueOf(view.getTitleTextField().getText());
                    String ratingString = String.valueOf(view.getRatingTextField().getText());
                    String caloriesString = String.valueOf(view.getCaloriesTextField().getText());
                    String proteinString = String.valueOf(view.getProteinTextField().getText());
                    String fatString = String.valueOf(view.getFatTextField().getText());
                    String sodiumString = String.valueOf(view.getSodiumTextField().getText());
                    String priceString = String.valueOf(view.getPriceTextField().getText());

                    double rating = 0.0, price = 0.0;
                    int calories = 0, protein = 0, fat = 0, sodium = 0;
                    try {
                        rating = Double.parseDouble(ratingString);
                        calories = Integer.parseInt(caloriesString);
                        protein = Integer.parseInt(proteinString);
                        fat = Integer.parseInt(fatString);
                        sodium = Integer.parseInt(sodiumString);
                        price = Double.parseDouble(priceString);

                        if (Start.getDeliveryService().modifyMenuItem(initialTitle, title, rating, calories, protein, fat, sodium, price)) {
                            view.initializeTable(Start.getDeliveryService().getMenuItems());
                            JOptionPane.showMessageDialog(this.view, "Successfully modified!");
                        } else {
                            JOptionPane.showMessageDialog(this.view, "Composite product can not change its fields!", "ERROR", JOptionPane.ERROR_MESSAGE);
                        }
                    } catch (NumberFormatException exception) {
                        JOptionPane.showMessageDialog(this.view, "ILLEGAL ARGUMENTS", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(this.view, "Please select a single row to modify and then modify in the text fields!", "ERROR Modify product", JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "ADD_BASE_PRODUCT": {
                String title = String.valueOf(view.getTitleTextField().getText());
                String ratingString = String.valueOf(view.getRatingTextField().getText());
                String caloriesString = String.valueOf(view.getCaloriesTextField().getText());
                String proteinString = String.valueOf(view.getProteinTextField().getText());
                String fatString = String.valueOf(view.getFatTextField().getText());
                String sodiumString = String.valueOf(view.getSodiumTextField().getText());
                String priceString = String.valueOf(view.getPriceTextField().getText());

                if (title.equals("")) {
                    JOptionPane.showMessageDialog(this.view, "ILLEGAL TITLE", "ERROR", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                double rating = 0.0, price = 0.0;
                int calories = 0, protein = 0, fat = 0, sodium = 0;
                try {
                    rating = Double.parseDouble(ratingString);
                    calories = Integer.parseInt(caloriesString);
                    protein = Integer.parseInt(proteinString);
                    fat = Integer.parseInt(fatString);
                    sodium = Integer.parseInt(sodiumString);
                    price = Double.parseDouble(priceString);

                    if (Start.getDeliveryService().addBaseProduct(title, rating, calories, protein, fat, sodium, price)) {
                        view.initializeTable(Start.getDeliveryService().getMenuItems());
                        JOptionPane.showMessageDialog(this.view, "Successfully added base product!");
                    } else {
                        JOptionPane.showMessageDialog(this.view, "Can not add this base product!", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(this.view, "ILLEGAL ARGUMENTS", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case "ADD_COMPOSITE_PRODUCT": {
                List<String> composedOf = new ArrayList<>();
                String title = String.valueOf(view.getTitleTextField().getText());
                if (view.getTable().getSelectedRowCount() > 0) {
                    if (title.equals("")) {
                        JOptionPane.showMessageDialog(this.view, "ILLEGAL TITLE", "ERROR", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    int[] selectedRow = view.getTable().getSelectedRows();
                    for (int i : selectedRow) {
                        composedOf.add(view.getTable().getValueAt(i, 0).toString());
                    }
                    if (Start.getDeliveryService().addCompositeProduct(title, composedOf)) {
                        view.initializeTable(Start.getDeliveryService().getMenuItems());
                        JOptionPane.showMessageDialog(this.view, "Successfully added composite product!");
                    } else {
                        JOptionPane.showMessageDialog(this.view, "Can not add this composite product!", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(this.view, "Select alt least one row!", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case "GENERATE_FIRST_REPORT": {
                String firstTextField = String.valueOf(view.getReportFirstTextField().getText());
                String secondTextField = String.valueOf(view.getReportSecondTextField().getText());
                int startHour = 0, endHour = 0;
                try {
                    startHour = Integer.parseInt(firstTextField);
                    endHour = Integer.parseInt(secondTextField);
                    if (startHour < 0 || endHour < 0 || startHour > 24 || endHour > 24) {
                        throw new NumberFormatException();
                    }
                    if (startHour < endHour) {
                        List<Order> ordersPerfBetween = Start.getDeliveryService().firstReport(startHour, endHour);
                        JOptionPane.showMessageDialog(this.view, "First report successfully created!");
                    } else {
                        JOptionPane.showMessageDialog(this.view, "First number should be smaller than the second", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(this.view, "Introduce two numbers between 0 and 24", "ERROR", JOptionPane.ERROR_MESSAGE);
                }

                break;
            }
            case "GENERATE_SECOND_REPORT": {
                String firstTextField = String.valueOf(view.getReportFirstTextField().getText());
                int nrTimes = 0;
                try {
                    nrTimes = Integer.parseInt(firstTextField);
                    if (nrTimes < 0) {
                        throw new NumberFormatException();
                    }
                    List<MenuItem> diffProductsOrdered = Start.getDeliveryService().secondReport(nrTimes);
                    JOptionPane.showMessageDialog(this.view, "Second report successfully created!");
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(this.view, "Introduce a number in first text field", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case "GENERATE_THIRD_REPORT": {
                String firstTextField = String.valueOf(view.getReportFirstTextField().getText());
                String secondTextField = String.valueOf(view.getReportSecondTextField().getText());
                int nrTimes = 0;
                double amount = 0.0;
                try {
                    nrTimes = Integer.parseInt(firstTextField);
                    amount = Double.parseDouble(secondTextField);
                    if (nrTimes < 0 || amount < 0) {
                        throw new NumberFormatException();
                    }
                    List<String> clients = Start.getDeliveryService().thirdReport(nrTimes, amount);
                    JOptionPane.showMessageDialog(this.view, "Third report successfully created!");
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(this.view, "ILLEGAL ARGUMENTS\nYou should introduce positive numbers", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case "GENERATE_FOURTH_REPORT": {
                String firstTextField = String.valueOf(view.getReportFirstTextField().getText());
                int daySpecified = 0;
                try {
                    daySpecified = Integer.parseInt(firstTextField);
                    if (daySpecified < 1 || daySpecified > 31) {
                        throw new NumberFormatException();
                    }
                    List<String> products = Start.getDeliveryService().fourthReport(daySpecified);
                    JOptionPane.showMessageDialog(this.view, "Fourth report successfully created!");
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(this.view, "Introduce a number between 1 and 31 in first text field", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
        }
    }
}
