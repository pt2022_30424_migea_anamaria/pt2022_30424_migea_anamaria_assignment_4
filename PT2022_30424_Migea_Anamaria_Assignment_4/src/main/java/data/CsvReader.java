package data;

import business.BaseProduct;
import business.MenuItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class CsvReader {
    private static final String SEPARATOR = ",";

    private final Reader source;

    public CsvReader(Reader source) {
        this.source = source;
    }
    
    public List<String> readHeader() {
        try (BufferedReader reader = new BufferedReader(source)) {
            return reader.lines()
                    .findFirst()
                    .map(line -> Arrays.asList(line.split(SEPARATOR)))
                    .get();
        } catch (IOException e) {
            return null;
        }
    }
    
    public List<MenuItem> readRecords() {
        List<MenuItem> baseProducts;
        try (BufferedReader reader = new BufferedReader(source)) {
            baseProducts = reader.lines()
                    .skip(1)
                    .map(line -> Arrays.asList(line.split(SEPARATOR)))
                    .map(l -> new BaseProduct((new StringBuilder()).append(l.get(0)).deleteCharAt(l.get(0).length()-1).toString(),Double.parseDouble(l.get(1)),Integer.parseInt(l.get(2)),Integer.parseInt(l.get(3)),Integer.parseInt(l.get(4)),Integer.parseInt(l.get(5)),Double.parseDouble(l.get(6))))
                    .distinct()
                    .collect(Collectors.toList());
            return baseProducts;
        } catch (IOException | NoSuchElementException e) {
            return null;
        }
    }
}
