package data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileOut {

    private String name;

    public FileOut(String name) {
        this.name = name;
    }

    public void createFile(){
        try {
            File myFile = new File(name);
            if (myFile.createNewFile()) {
                System.out.println("File created: " + myFile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void writeToFile(String string){
        try {
            FileWriter myWriter = new FileWriter(name);
            myWriter.write(string);
            myWriter.close();
            //System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
