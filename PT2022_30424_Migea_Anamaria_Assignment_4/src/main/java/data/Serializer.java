package data;

import business.Client;
import business.MenuItem;
import business.Order;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Serializer<T> {
    private static final String CLIENTS_PATH = "src/main/resources/clients.ser";
    private static final String MENU_ITEMS_PATH = "src/main/resources/menuitems.ser";
    private static final String ORDERS_PATH = "src/main/resources/orders.ser";

    private final Class<T> type;

    public Serializer(Class<T> type) {
        this.type = type;
    }

    public void serializeObjects(List<T> tList){
        try {
            String path="";
            if(type.isAssignableFrom(Client.class)){
                path=CLIENTS_PATH;
            }else if(type.isAssignableFrom(MenuItem.class)){
                path=MENU_ITEMS_PATH;
            }
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(tList);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public List<T> deserializeObjects(){
        String path="";
        if(type.isAssignableFrom(Client.class)){
            path=CLIENTS_PATH;
        }else if(type.isAssignableFrom(MenuItem.class)){
            path=MENU_ITEMS_PATH;
        }

        List<T> clients =  new ArrayList<>();
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            clients = (List<T>) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException | ClassNotFoundException i) {
            return clients;
        }

        return clients;
    }

    public void serializeOrders(HashMap<Order, ArrayList<MenuItem>> orders){
        try {
            String path=ORDERS_PATH;

            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(orders);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public HashMap<Order, ArrayList<MenuItem>> deserializeOrders(){
        String path=ORDERS_PATH;

        HashMap<Order, ArrayList<MenuItem>> orders = new HashMap<>();
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            orders = (HashMap<Order, ArrayList<MenuItem>>) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException | ClassNotFoundException i) {
            return orders;
        }

        return orders;
    }

}
