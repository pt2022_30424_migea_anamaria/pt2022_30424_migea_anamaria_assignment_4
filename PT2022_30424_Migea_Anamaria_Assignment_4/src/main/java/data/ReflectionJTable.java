package data;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.lang.reflect.Field;
import java.util.List;

public class ReflectionJTable {

    public JTable retrieveTable(List<MenuItem> listObjects) {

        String[] col = new String[7];
        col[0]="title";
        col[1]="rating";
        col[2]="calories";
        col[3]="proteins";
        col[4]="fat";
        col[5]="sodium";
        col[6]="price";

        Object[][] data = new Object[listObjects.size()][col.length];
        int indexData = 0;
        for(Object object: listObjects){
            int j = 0;
            col[j] = "title";
            data[indexData][j] = ((MenuItem)object).getTitle();
            j++;
            if(object instanceof BaseProduct){
                for (Field field : BaseProduct.class.getDeclaredFields()) {
                    field.setAccessible(true); // set modifier to public
                    Object value;
                    try {
                        col[j]=field.getName();
                        value = field.get(object);
                        data[indexData][j] = value;
                        j++;

                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }else if(object instanceof CompositeProduct){
                col[j]="rating";
                data[indexData][j] = ((CompositeProduct) object).computeRating();
                j++;
                col[j]="calories";
                data[indexData][j] = ((CompositeProduct) object).computeCalories();
                j++;
                col[j]="proteins";
                data[indexData][j] = ((CompositeProduct) object).computeProtein();
                j++;
                col[j]="fat";
                data[indexData][j] = ((CompositeProduct) object).computeFat();
                j++;
                col[j]="sodium";
                data[indexData][j] = ((CompositeProduct) object).computeSodium();
                j++;
                col[j]="price";
                data[indexData][j] = ((CompositeProduct) object).computePrice();
            }
            indexData++;
        }
        TableModel model = new DefaultTableModel(data, col)
        {
            public boolean isCellEditable(int row, int column)
            {
                return false;//This causes all cells to be not editable
            }
        };
        JTable table = new JTable(model);
        return table;
    }

}
