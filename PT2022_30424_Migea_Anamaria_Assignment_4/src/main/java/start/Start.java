package start;

import business.DeliveryService;
import presentation.View;

import javax.swing.*;


public class Start {

    private static DeliveryService deliveryService;

    public static void main(String[] args) {
        deliveryService = new DeliveryService();
        deliveryService.exportClients();
        deliveryService.exportMenu();
        deliveryService.exportOrders();

        //System.out.println(deliveryService.exportMenu().size());
        JFrame frame = new View("Food delivery management system");
        frame.setVisible(true);

    }

    public static DeliveryService getDeliveryService() {
        return deliveryService;
    }
}
