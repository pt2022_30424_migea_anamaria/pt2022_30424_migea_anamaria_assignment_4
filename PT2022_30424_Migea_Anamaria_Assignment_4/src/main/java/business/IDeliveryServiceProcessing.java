package business;

import java.util.List;

/**
 * Delivery processing interface g containing the main operations that can
 * be executed by the administrator and client
 */
public interface IDeliveryServiceProcessing {

    public List<MenuItem> importInitialProducts();

    public List<MenuItem> exportMenu();

    public List<Client> exportClients();

    public void exportOrders();

    public void removeMenuItem(String title);

    public boolean modifyMenuItem(String title,String newTitle,double rating,int calories,int protein,int fat,int sodium,double price);

    public boolean addBaseProduct(String title,double rating,int calories,int protein,int fat,int sodium,double price);

    public void addClient(Client newClient);

    public boolean addCompositeProduct(String title,List<String> composedOf);

    public List<MenuItem> searchByCriteria(String title,double rating,int calories,int protein,int fat,int sodium,double price);

    public void addOrder(Client client, List<String> composedOf);

    public List<Order> firstReport(int startHour, int endHour);

    public List<MenuItem> secondReport(int nrOfTimes);

    public List<String> thirdReport(int nrOfTimes, double amount);

    public List<String> fourthReport(int daySpecified);

}
