package business;


import data.*;
import start.Start;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Delivery service class modeling the delivery service company
 */
public class DeliveryService extends Observable implements IDeliveryServiceProcessing, Serializable {

    private final List<MenuItem> menuItems = new ArrayList<>();

    private final List<Client> clients = new ArrayList<>();

    private final HashMap<Order, ArrayList<MenuItem>> orders = new HashMap<>();

    private final Serializer<MenuItem> serializerMenuItems =  new Serializer<>(MenuItem.class);

    private final Serializer<Client> serializerClients =  new Serializer<>(Client.class);

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public List<Client> getClients() {
        return clients;
    }

    public HashMap<Order, ArrayList<MenuItem>> getOrders() {
        return orders;
    }

    /**
     * <p> wellFormed method ... class invariant that should always be true before and after any method completes
     * </p>
     * @invariant each order should contain at least one menu item
     * @return always true
     */
    private boolean wellFormed(){
        return orders.entrySet().stream().allMatch(entry -> entry.getValue().size()>0);
    }

    /**
     * <p>importInitialProducts method ... import the initial set of products which will populate the menu from a .csv file
     * </p>
     * @return imported base products
     */
    @Override
    public List<MenuItem> importInitialProducts(){
        List<MenuItem> importedMenuItems;
        Reader reader = null;
        try {
            reader = new FileReader("src/main/resources/products.csv");
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        CsvReader csvReader = new CsvReader(reader);
        importedMenuItems = csvReader.readRecords();

        menuItems.clear();
        addMenuItems(importedMenuItems);
        serializerMenuItems.serializeObjects(importedMenuItems);

        return importedMenuItems;
    }

    /**
     * <p> exportMenu method ... deserialize the menu and populates the list of menu items
     * </p>
     * @return list of products in the menu
     */
    @Override
    public List<MenuItem> exportMenu(){
        List<MenuItem> menuItems;
        menuItems = serializerMenuItems.deserializeObjects();
        addMenuItems(menuItems);
        return menuItems;
    }

    /**
     * <p> exportClients method ... deserialize the clients and populates the list of clients
     * </p>
     * @return list of users with an account
     */
    @Override
    public List<Client> exportClients(){
        List<Client> clients;
        clients = serializerClients.deserializeObjects();
        addClients(clients);
        return clients;
    }

    /**
     * <p> exportOrders method ... deserialize the orders and populates the hashmap with te previously
     * performed orders
     * </p>
     */
    @Override
    public void exportOrders(){
        orders.putAll(serializerMenuItems.deserializeOrders());
    }

    /**
     * <p>
     * addMenuItems method ...  adds menu items to the menu
     * </p>
     * @pre added menu items can not be null
     * @param menuItems tot add to the menu
     */
    public void addMenuItems(List<MenuItem> menuItems){
        assert menuItems!=null;
        for(MenuItem menuItem: menuItems){
            if(!this.menuItems.contains(menuItem)){
                this.menuItems.add(menuItem);
            }
        }
    }

    /**
     * <p>
     * addClients method ...  adds clients to the delivery service company
     * </p>
     * @pre added clients can not be null
     * @param clients tot add to the company
     */
    public void addClients(List<Client> clients){
        assert clients!=null;
        for(Client client: clients){
            if(!this.clients.contains(client)){
                this.clients.add(client);
            }
        }
    }

    /**
     * <p> removeMenuItem method ... removes an item  from the menu based on its title
     * </p>
     * @param title of the menu item
     */
    @Override
    public void removeMenuItem(String title){
        menuItems.removeIf(menuItem -> menuItem.getTitle().equals(title));
        serializerMenuItems.serializeObjects(menuItems);
    }

    /**
     * <p> modifyMenuItem method ... modifies an item  from the menu
     * </p>
     * @param title old title of the item
     * @param newTitle new title of the item
     * @param rating new rating
     * @param calories new calories
     * @param protein new protein
     * @param fat new fat
     * @param sodium new sodium
     * @param price new price
     * @return true if success, else false
     */
    @Override
    public boolean modifyMenuItem(String title,String newTitle,double rating,int calories,int protein,int fat,int sodium,double price){
        for(MenuItem menuItem:menuItems){
            if(menuItem.getTitle().equals(title)){
                if(menuItem instanceof BaseProduct){
                    menuItem.setTitle(newTitle);
                    ((BaseProduct) menuItem).setRating(rating);
                    ((BaseProduct) menuItem).setCalories(calories);
                    ((BaseProduct) menuItem).setProtein(protein);
                    ((BaseProduct) menuItem).setFat(fat);
                    ((BaseProduct) menuItem).setSodium(sodium);
                    ((BaseProduct) menuItem).setPrice(price);
                    serializerMenuItems.serializeObjects(menuItems);
                    return true;
                }else if(!title.equals(newTitle)){
                    menuItem.setTitle(newTitle);
                    serializerMenuItems.serializeObjects(menuItems);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * <p> addBaseProduct method ... adds a new base product to the menu
     *</p>
     * @return true if success, else false
     */
    @Override
    public boolean addBaseProduct(String title,double rating,int calories,int protein,int fat,int sodium,double price){
        BaseProduct baseProduct = new BaseProduct(title,rating,calories,protein,fat,sodium,price);
        if(findByTitle(title)==null){
            menuItems.add(baseProduct);
            serializerMenuItems.serializeObjects(menuItems);
            return true;
        }
        return false;
    }

    /**
     * <p> userExists method ... checks if a user with a specified username exists in the system
     * </p>
     * @param username name of the client
     * @invariant all the clients successfully registered must have different usernames
     * @return the client having that username if exists, else null
     */
    public Client userExists(String username){
        for(Client client: clients){
            if(client.getUsername().equals(username)){
                return client;
            }
        }
        return null;
    }

    /**
     * <p> addClient method ... adds a new client tot the system
     * </p>
     * @pre the new added client has a different username as those already existing clients
     * @param newClient client to be added to the system
     */
    @Override
    public void addClient(Client newClient){
        assert userExists(newClient.getUsername())==null;
        clients.add(newClient);
        serializerClients.serializeObjects(clients);
    }

    /**
     * <p> findByTitle method ... checks if a menu item with a specified title exists in the menu
     * </p>
     * @param title name of the menu item
     * @return the menu item having that title if exists, else null
     */
    private MenuItem findByTitle(String title){
        MenuItem menuItemFind = menuItems.stream()
                .filter(menuItem -> menuItem.getTitle().equals(title))
                .findAny()
                .orElse(null);
        return menuItemFind;
    }

    /**
     * <p> addCompositeProduct method ... adds a new composite product to the menu
     *</p>
     * @return true if success, else false
     */
    @Override
    public boolean addCompositeProduct(String title,List<String> composedOf){
        List<MenuItem> composedOfMenuItems = new ArrayList<>();
        for(String s:composedOf){
            composedOfMenuItems.add(findByTitle(s));
        }
        CompositeProduct compositeProduct = new CompositeProduct(title,composedOfMenuItems);
        MenuItem menuItemFind = findByTitle(title);
        if(menuItemFind==null){
            menuItems.add(compositeProduct);
            serializerMenuItems.serializeObjects(menuItems);
            return true;
        }
        return false;
    }

    /**
     * <p> searchByCriteria method ... search for products based on one or multiple criteria
     * </p>
     * @return a list of menu items which have the specified criteria
     */
    @Override
    public List<MenuItem> searchByCriteria(String title,double rating,int calories,int protein,int fat,int sodium,double price){
        List<MenuItem> foundMenuItems;

        foundMenuItems = menuItems.stream()
                .filter(menuItem -> menuItem.getTitle().toLowerCase().contains(title) && !title.equals("")
                        || menuItem.computeRating() <= rating
                        || menuItem.computeCalories() <= calories
                        || menuItem.computeProtein() <= protein
                        || menuItem.computeFat() <= fat
                        || menuItem.computeSodium() <= sodium
                        || menuItem.computePrice() <= price)
                .collect(Collectors.toList());

        return  foundMenuItems;
    }

    /**
     * <p> addOrder method ... adds a new order
     * </p>
     * @param client that performed the order
     * @param composedOf names of products the ordered
     * @pre client performing the order can not be null
     * @pre the list of products ordered can not be null
     * @post each order should contain at least one product
     */
    @Override
    public void addOrder(Client client, List<String> composedOf) {
        assert client!=null : "Client is null";
        assert composedOf!=null;
        ArrayList<MenuItem> items = new ArrayList<>();
        AtomicReference<Double> totalPrice = new AtomicReference<>(0.0);
        //search for title in menuitems list
        composedOf.forEach(entity -> {
            List<MenuItem> result = Start.getDeliveryService().getMenuItems().stream()
                    .filter(item -> item.getTitle().equals(entity))
                    .collect(Collectors.toList());
            if(result.size() != 0){
                items.add(result.get(0));
                totalPrice.set(totalPrice.get() + result.get(0).computePrice());
            }
        });
        Order order = new Order(client);
        orders.put(order, items);
        FileOut fileOut = new FileOut("src/main/resources/bill" + order.getOrderID() + ".txt");
        fileOut.createFile();

        StringBuilder toWrite = new StringBuilder("Order by client: " + clients.stream()
                .filter(client1 -> client1.getUsername().equals(client.getUsername()))
                .findAny().get().getUsername() + "\n");
        toWrite.append("Date: ").append(order.getOrderDate()).append("\n");
        toWrite.append("Ordered items: ").append(items.stream()
                .map(MenuItem::getTitle)
                .collect(Collectors.joining(", "))).append("\n");
        toWrite.append("Total price: ").append(totalPrice).append("\n");
        fileOut.writeToFile(toWrite.toString());

        serializerMenuItems.serializeOrders(orders);
        setChanged();
        notifyObservers(order);

        assert wellFormed();
    }

    /**
     * <p>firstReport method. . . time interval of the orders – a report should be generated with the orders performed
     * between a given start hour and a given end hour regardless the date.
     * </p>
     * @param startHour the start hour
     * @param endHour the end hour
     * @return orders performed between the given start and end hour
     */
    @Override
    public List<Order> firstReport(int startHour, int endHour){
        List<Order> ordersPerfBetween = new ArrayList<>(orders.keySet());
        ordersPerfBetween = ordersPerfBetween.stream()
                .filter(order -> order.getOrderDate().getHours() >= startHour && order.getOrderDate().getHours() <= endHour)
                .collect(Collectors.toList());

        FileOut fileOut = new FileOut("src/main/resources/firstreport.txt");
        fileOut.createFile();
        StringBuilder toWrite = new StringBuilder("First report\n");
        ordersPerfBetween.forEach(order -> toWrite.append(order.toString()).append("\n"));
        fileOut.writeToFile(toWrite.toString());

        return ordersPerfBetween;
    }

    /**
     * <p>secondReport method. . . the products ordered more than a specified number of times so far.
     *      * </p>
     * @param nrOfTimes specified number of times
     * @return products ordered more than the specified number of times
     */
    @Override
    public List<MenuItem> secondReport(int nrOfTimes){
        List<MenuItem> diffProductsOrdered = new ArrayList<>();
        List<MenuItem> productsOrdered = new ArrayList<>();
        productsOrdered = orders.values().stream().flatMap(Collection::stream)
                .collect(Collectors.toList());

        productsOrdered.stream().collect(
                        Collectors.groupingBy(
                                Function.identity(), Collectors.counting()
                        )
                )
                .entrySet().stream()
                .filter(entry -> entry.getValue() >= nrOfTimes)
                .forEach(entry -> diffProductsOrdered.add(entry.getKey()));

        FileOut fileOut = new FileOut("src/main/resources/secondreport.txt");
        fileOut.createFile();
        StringBuilder toWrite = new StringBuilder("Second report\n");
        diffProductsOrdered.forEach(menuItem -> toWrite.append(menuItem.getTitle()).append("\n"));
        fileOut.writeToFile(toWrite.toString());

        return diffProductsOrdered;
    }

    /**
     * <p>thirdReport method. . . the clients that have ordered more than a specified number of times so far and the
     *     value of the order was higher than a specified amount.
     * </p>
     * @param nrOfTimes specified number of times
     * @param amount specified amount
     * @return clients
     */
    @Override
    public List<String> thirdReport(int nrOfTimes, double amount){
        List<String> clients = new ArrayList<>();

        List<Order> ordersMade = new ArrayList<>(orders.keySet());
        List<String> diffClients = new ArrayList<>();

        ordersMade.stream().collect(
                        Collectors.groupingBy(
                                Order::getClientUsername, Collectors.counting()
                        )
                )
                .entrySet().stream()
                .filter(entry -> entry.getValue() >= nrOfTimes)
                .forEach(entry -> diffClients.add(entry.getKey()));


        Map<String, Long> clientsWithValueOfOrderHigher = ordersMade.stream()
                .filter(order -> orders.get(order).stream().mapToDouble(MenuItem::computePrice).sum() > amount)
                .collect(Collectors.groupingBy(Order::getClientUsername, Collectors.counting()));

        //make AND between 2 lists
        clients = clientsWithValueOfOrderHigher.keySet().stream()
                .filter(diffClients::contains)
                .collect(Collectors.toList());

        FileOut fileOut = new FileOut("src/main/resources/thirdreport.txt");
        fileOut.createFile();
        StringBuilder toWrite = new StringBuilder("Third report\n");
        clients.forEach(s -> toWrite.append(s).append("\n"));
        fileOut.writeToFile(toWrite.toString());

        return clients;
    }

    /**
     * <p>fourthReport method. . . the products ordered within a specified day with the number of times they have
     *     been ordered.
     * </p>
     * @param daySpecified the specified day
     * @return the products along with their number of times that have been ordered
     */
    @Override
    public List<String> fourthReport(int daySpecified){

        List<String> list = new ArrayList<>();

        Map<Order, ArrayList<MenuItem>> ordersOfTheDay = orders.entrySet().stream()
                                                        .filter(entry -> entry.getKey().getCalendar().get(Calendar.DAY_OF_MONTH) == daySpecified)
                                                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        ordersOfTheDay.values().stream()
                .flatMap(Collection::stream)
                .collect(
                        Collectors.groupingBy(
                                Function.identity(), Collectors.counting()
                        )
                )
                .forEach((product, nrTimesOrdered) -> list.add(product.getTitle() + " was ordered " + nrTimesOrdered + " times"));

        FileOut fileOut = new FileOut("src/main/resources/fourthreport.txt");
        fileOut.createFile();
        StringBuilder toWrite = new StringBuilder("Fourth report\n");
        list.forEach(s -> toWrite.append(s).append("\n"));
        fileOut.writeToFile(toWrite.toString());

        return list;
    }

}
