package business;

import java.util.List;

public class CompositeProduct extends MenuItem{
    private List<MenuItem> composedOfMenuItems;


    public CompositeProduct(String title, List<MenuItem> composedOfMenuItems) {
        super(title);
        this.composedOfMenuItems = composedOfMenuItems;
    }

    @Override
    public double computeRating() {
        double rating = 0.0;
        for(MenuItem menuItem: composedOfMenuItems){
            rating = rating + menuItem.computeRating();
        }
        return rating/composedOfMenuItems.size();
    }

    @Override
    public int computeCalories() {
        int calories = 0;
        for(MenuItem menuItem: composedOfMenuItems){
            calories = calories + menuItem.computeCalories();
        }
        return calories;
    }

    @Override
    public int computeProtein() {
        int protein = 0;
        for(MenuItem menuItem: composedOfMenuItems){
            protein = protein + menuItem.computeProtein();
        }
        return protein;
    }

    @Override
    public int computeFat() {
        int fat = 0;
        for(MenuItem menuItem: composedOfMenuItems){
            fat = fat + menuItem.computeFat();
        }
        return fat;
    }

    @Override
    public int computeSodium() {
        int sodium = 0;
        for(MenuItem menuItem: composedOfMenuItems){
            sodium = sodium + menuItem.computeSodium();
        }
        return sodium;
    }

    @Override
    public double computePrice() {
        double price = 0.0;
        for(MenuItem menuItem: composedOfMenuItems){
            price = price + menuItem.computePrice();
        }
        return price;
    }

}
