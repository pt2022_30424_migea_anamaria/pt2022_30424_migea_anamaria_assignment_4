package business;

import start.Start;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable {

    private int orderID;
    private String clientUsername;
    private Date orderDate;
    private Calendar calendar;

    public Order(Client client) {
        Start.getDeliveryService().getOrders().keySet().stream()
                .max(Comparator.comparingInt(Order::getOrderID))
                .ifPresentOrElse(order -> orderID = order.getOrderID() + 1, () -> orderID = 0);
        this.orderDate = new Date(System.currentTimeMillis());
        this.clientUsername = client.getUsername();
        this.calendar = Calendar.getInstance();
        calendar.setTime(this.orderDate);
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getClientUsername() {
        return clientUsername;
    }

    public void setClientUsername(String clientUsername) {
        this.clientUsername = clientUsername;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderID == order.orderID && clientUsername.equals(order.clientUsername) && orderDate.equals(order.orderDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, clientUsername, orderDate);
    }

    @Override
    public String toString() {
        return "Order{ " +
                "orderID=" + orderID +
                ", clientUsername='" + clientUsername + '\'' +
                ", orderDate=" + orderDate +
                " } created\n";
    }
}
